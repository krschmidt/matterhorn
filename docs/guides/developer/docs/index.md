Opencast Development Guides
===========================

These guides will help you if you want to participate in Opencast development.


 - [Development Process](development-process.md)
    - [Reviewing, Merging and Declining Pull Requests](reviewing-and-merging.md)
    - [Release Manager](release-manager.md)
 - [Development Environment](development-environment.md)
 - [Proposal Log](proposal-log.md)
 - [Licenses and Legal Matters](license.md)
 - [Localization](localization.md)
 - [Packaging Guidelines](packaging.md)
 - [Modules](modules/index.md)
    - [Admin UI](modules/admin-ui.md)
    - [Stream Security](modules/stream-security.md)
    - […](modules/index.md)
 - [Project Infrastructure](infrastructure/index.md)
    - [Nexus Maven Repository](infrastructure/nexus.md)

